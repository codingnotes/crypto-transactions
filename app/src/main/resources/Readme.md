# Importing transactions

To import transactions, deposits and withdraw history from exchanges follow the instructions below.

## Kraken 

1. Go to [History -> Export](https://www.kraken.com/u/history/export)
2. Select Ledgers from the drop down menu
3. Choose required dates
4. Select all fields to be included in the report and hit submit

## Binance

To export trades - **max 3 months back**:
1. Go to [Orders -> Trade History](https://www.binance.com/userCenter/tradeHistory.html)
2. Click `Export Complete Trade History`

To export withdrawal/deposit history: 
1. Go to [Funds -> History](https://www.binance.com/userCenter/transactionHistory.html)
2. Click `Export Complete Deposit/Withdrawal History`
3. Rename files from csv to xlsx

## Bittrex

1. Go to [Orders](https://bittrex.com/History)
2. Click `Load all` to download the file

Deposits and withdrawals export is not available yet. 
Prepare files manually

## Bitfinex

To export trades- **max 3 months back**:
1. Go to [Reports](https://www.bitfinex.com/reports)
2. Click on Trade History
3. Export withdrawals/deposits from the same page

## Bitstamp

1. Go to [Account -> Transactions](https://www.bitstamp.net/account/transactions/)
2. Click `Export all`


 
