package io.github.iluu.crypto.transactions.services

import io.github.iluu.crypto.transactions.imports.parsers.ParsersFactory
import io.github.iluu.crypto.transactions.repositories.TransactionRepository
import org.jetbrains.annotations.NotNull
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.io.FileOutputStream


@Service
class ImportService(@Autowired private val parsersFactory: ParsersFactory,
                    @Autowired private val transactionRepository: TransactionRepository) {

    private val logger = LoggerFactory.getLogger(this.javaClass)

    fun consumeFile(@NotNull file: MultipartFile) {
        logger.debug("Received file '${file.originalFilename}'")

        val parser = parsersFactory.parserByFileName(file.originalFilename.orEmpty())
        val tempFile = createTemporaryFile(file)

        logger.debug("Parser found: `$parser` temp file: ${tempFile.name}")
        parser?.parse(tempFile)?.apply {
            transactionRepository.saveAll(toList())
        }

        tempFile.delete()
    }

    private fun createTemporaryFile(file: MultipartFile): File {
        val temp = File(file.originalFilename)
        temp.createNewFile()

        val fos = FileOutputStream(temp)
        fos.write(file.bytes)
        fos.close()
        return temp
    }

}