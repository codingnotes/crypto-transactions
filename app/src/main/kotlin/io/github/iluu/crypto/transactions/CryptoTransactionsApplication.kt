package io.github.iluu.crypto.transactions

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CryptoTransactionsApplication

fun main(args: Array<String>) {
    runApplication<CryptoTransactionsApplication>(*args)
}