package io.github.iluu.crypto.transactions.controllers

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/")
class AuthController {

    @GetMapping
    fun hello(): String {
        return "Hello World"
    }

}
