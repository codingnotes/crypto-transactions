package io.github.iluu.crypto.transactions.controllers

import io.github.iluu.crypto.transactions.domain.Transaction
import io.github.iluu.crypto.transactions.repositories.TransactionRepository
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/transactions")
class TransactionController(val repository: TransactionRepository) {

    @GetMapping
    fun findAll() = repository.findAll()

    @PostMapping
    fun addTransaction(@RequestBody transaction: Transaction) = repository.save(transaction)

}