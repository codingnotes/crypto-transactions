package io.github.iluu.crypto.transactions.controllers

import io.github.iluu.crypto.transactions.services.ImportService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import java.io.IOException

@RestController
@RequestMapping("/upload")
class ImportController(@Autowired private val importService: ImportService) {

    @PostMapping
    fun fileUpload(@RequestParam("file") file: MultipartFile,
                   redirectAttributes: RedirectAttributes): String {
        if (file.isEmpty) {
            redirectAttributes.addFlashAttribute("message", "Please select file to upload")
        } else {
            try {
                importService.consumeFile(file)
                redirectAttributes.addFlashAttribute("message", "Successfully uploaded '${file.originalFilename}'")
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return "redirect:uploadStatus"
    }

}