# Quick Dev API Guide

All examples below are done using [httpie](https://httpie.org/).
All examples also assume app a debug app is deployed at `localhost:8080`

## Upload

Uploading a file from your home directory:
```bash
http -f POST localhost:8080/upload file@~/test.txt
```

Uploading a file from current directory:
```bash
http -f POST localhost:8080/upload file@`pwd`/test.txt
```

## Transactions

Listing all transactions in the database:
```
http localhost:8080/transactions
```

Response will return an array containing transaction metadata:
```
[
    {
        "buyPrice": null,
        "comment": null,
        "exchange": null,
        "fee": null,
        "id": 1,
        "importTime": "2018-11-01T12:13:42.430+0000",
        "importedFrom": null,
        "sellPrice": null,
        "tradeTime": "2018-07-25T09:08:22.000+0000",
        "type": "TRADE"
    }
]
```

Creating new transaction:
```
http POST localhost:8080/transactions tradeTime=2018-07-25'T'07:08:22.000Z
```
