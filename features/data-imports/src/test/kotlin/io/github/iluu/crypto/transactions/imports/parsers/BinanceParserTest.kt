package io.github.iluu.crypto.transactions.imports.parsers

import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class BinanceParserTest {

    @Test
    fun `parses buy price correctly`() {
        val parser = BinanceParser()
        val trx = parser.parseTrx("2018-03-13 21:48:51,WTC,3.796", "deposits.csv")

        trx.buyPrice!!.apply {
            assertThat(value, equalTo(3796L))
            assertThat(multiplier, equalTo(3))
            assertThat(asset, equalTo("WTC"))
        }
    }
}