package io.github.iluu.crypto.transactions.imports.parsers

import io.github.iluu.crypto.transactions.domain.Price
import io.github.iluu.crypto.transactions.domain.Transaction
import io.github.iluu.crypto.transactions.domain.TransactionType
import io.github.iluu.crypto.transactions.domain.TransactionType.DEPOSIT
import io.github.iluu.crypto.transactions.domain.TransactionType.TRADE
import java.text.SimpleDateFormat

internal class BinanceParser : TransactionParser() {

    private val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

    override fun parseTrx(line: String, filename: String?): Transaction {
        val fields = line.split(",")
        return Transaction(
                tradeTime = dateFormat.parse(fields[0]),
                type = filename.orEmpty().parseTransactionType(),
                buyPrice = parsePrice(fields[1], fields[2]),
                importedFrom = filename
        )
    }

    private fun parsePrice(coin: String, value: String): Price {
        val multiplier = (value.length - 1) - value.indexOf(".")
        val parsedValue = value.replace(".", "").toLong()
        return Price(parsedValue, multiplier, coin)
    }

    private fun String.parseTransactionType(): TransactionType {
        return when {
            this.contains("deposits") -> DEPOSIT
            else -> TRADE
        }
    }

}
