package io.github.iluu.crypto.transactions.imports.parsers

import io.github.iluu.crypto.transactions.domain.Transaction
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.io.File

@Component
class ParsersFactory {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    fun parserByFileName(fileName: String): TransactionParser? {
        return when {
            fileName.startsWith(BINANCE, ignoreCase = true) -> BinanceParser()
            else -> {
                logger.error("Could not find a parser for '$fileName'")
                null
            }
        }
    }

    companion object {
        const val BINANCE = "binance-"
    }
}

abstract class TransactionParser {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    abstract fun parseTrx(line: String, filename: String?): Transaction

    open fun parse(file: File): Set<Transaction> {
        return mutableSetOf<Transaction>().apply {
            file.readLines().forEachIndexed { index, trx ->
                try {
                    if (index > 0) { // skip column names
                        add(parseTrx(trx, file.name))
                    }
                } catch (e: Exception) {
                    logger.error("Failed to parse transaction: `$index:${file.name}`")
                }
            }
        }
    }
}
