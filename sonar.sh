#!/usr/bin/env bash

./gradlew clean

sonar-scanner -X \
  -Dsonar.projectKey=codingnotes_crypto-transactions \
  -Dsonar.organization=codingnotes-bitbucket \
  -Dsonar.sources=. \
  -Dsonar.host.url=https://sonarcloud.io \
  -Dsonar.login=${SONAR_TOKEN}
