package io.github.iluu.crypto.transactions.repositories

import io.github.iluu.crypto.transactions.domain.Price
import io.github.iluu.crypto.transactions.domain.Transaction
import io.github.iluu.crypto.transactions.domain.TransactionType
import io.github.iluu.crypto.transactions.repositories.Columns.*
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.BatchPreparedStatementSetter
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.simple.SimpleJdbcInsert
import org.springframework.stereotype.Repository
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.text.SimpleDateFormat
import java.util.*

@Repository
class TransactionRepository(@Autowired private var jdbcTemplate: JdbcTemplate) {

    private val logger = LoggerFactory.getLogger(javaClass)
    private val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")

    fun save(transaction: Transaction) {
        SimpleJdbcInsert(jdbcTemplate)
                .withTableName("TRANSACTIONS")
                .apply {
                    setGeneratedKeyName("id")
                    execute(mapOf(
                            CREATED_AT.name to dateFormat.format(transaction.tradeTime),
                            ADDED_AT.name to dateFormat.format(Date()),
                            TYPE.name to transaction.type.ordinal,
                            BUY_VALUE.name to transaction.buyPrice?.value,
                            BUY_MULTIPLIER.name to transaction.buyPrice?.multiplier,
                            BUY_ASSET.name to transaction.buyPrice?.asset,
                            SELL_VALUE.name to transaction.sellPrice?.value,
                            SELL_MULTIPLIER.name to transaction.sellPrice?.multiplier,
                            SELL_ASSET.name to transaction.sellPrice?.asset,
                            FEE_VALUE.name to transaction.fee?.value,
                            FEE_MULTIPLIER.name to transaction.fee?.multiplier,
                            FEE_ASSET.name to transaction.fee?.asset,
                            EXCHANGE.name to transaction.exchange,
                            COMMENT.name to transaction.comment,
                            IMPORTED_FROM.name to transaction.importedFrom))
                }
    }

    fun findAll(): List<Transaction> =
            jdbcTemplate.query("select * from Transactions")
            { rs: ResultSet, _: Int ->
                Transaction(
                        rs.getLong("id"),
                        rs.getDateTime("created_at", dateFormat),
                        rs.getDateTime("added_at", dateFormat),
                        rs.getTransactionType(),
                        rs.getPrice("buy"),
                        rs.getPrice("sell"),
                        rs.getPrice("fee"),
                        rs.getString("exchange"),
                        rs.getString("comment"),
                        rs.getString("imported_from"))
            }

    fun saveAll(transactions: List<Transaction>) {
        val partitions = transactions.partition(BATCH_SIZE)
        logger.debug("Received: ${transactions.size} split into ${partitions.size}")
        for (batch in partitions) {
            jdbcTemplate.batchUpdate(SAVE_QUERY, object : BatchPreparedStatementSetter {

                override fun setValues(ps: PreparedStatement, idx: Int) {
                    ps.setString(CREATED_AT.idx, dateFormat.format(batch[idx].tradeTime))
                    ps.setString(ADDED_AT.idx, dateFormat.format(Date()))
                    ps.setInt(TYPE.idx, batch[idx].type.ordinal)
                    ps.setLong(BUY_VALUE.idx, batch[idx].buyPrice?.value ?: 0L)
                    ps.setInt(BUY_MULTIPLIER.idx, batch[idx].buyPrice?.multiplier ?: 0)
                    ps.setString(BUY_ASSET.idx, batch[idx].buyPrice?.asset)
                    ps.setLong(SELL_VALUE.idx, batch[idx].sellPrice?.value ?: 0L)
                    ps.setInt(SELL_MULTIPLIER.idx, batch[idx].sellPrice?.multiplier ?: 0)
                    ps.setString(SELL_ASSET.idx, batch[idx].sellPrice?.asset)
                    ps.setLong(FEE_VALUE.idx, batch[idx].fee?.value ?: 0L)
                    ps.setInt(FEE_MULTIPLIER.idx, batch[idx].fee?.multiplier ?: 0)
                    ps.setString(FEE_ASSET.idx, batch[idx].fee?.asset)
                    ps.setString(EXCHANGE.idx, batch[idx].exchange)
                    ps.setString(COMMENT.idx, batch[idx].comment)
                    ps.setString(IMPORTED_FROM.idx, batch[idx].importedFrom)
                }

                override fun getBatchSize() = batch.size
            })
        }
    }

    companion object {
        const val BATCH_SIZE = 500
        const val SAVE_QUERY =
                """INSERT INTO TRANSACTIONS (
                    CREATED_AT, ADDED_AT,
                    TYPE,
                    BUY_VALUE, BUY_MULTIPLIER, BUY_ASSET,
                    SELL_ASSET, SELL_MULTIPLIER, SELL_VALUE,
                    FEE_ASSET, FEE_MULTIPLIER, FEE_VALUE,
                    EXCHANGE, COMMENT, IMPORTED_FROM)
                   values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
                """
    }
}

private enum class Columns {
    CREATED_AT, ADDED_AT,
    TYPE,
    BUY_VALUE, BUY_MULTIPLIER, BUY_ASSET,
    SELL_ASSET, SELL_MULTIPLIER, SELL_VALUE,
    FEE_ASSET, FEE_MULTIPLIER, FEE_VALUE,
    EXCHANGE, COMMENT, IMPORTED_FROM;

    val idx = ordinal + 1
}

fun List<Transaction>.partition(partitionSize: Int): List<List<Transaction>> {
    val result = arrayListOf<List<Transaction>>()
    for (i in 0..size step partitionSize) {
        result.add(subList(i, if (i + partitionSize > size) size else i + partitionSize))
    }
    return result
}

fun ResultSet.getTransactionType() = TransactionType.values()[getInt("type")]

fun ResultSet.getPrice(prefix: String): Price? {
    val value = getLong(prefix + "_value")
    val multiplier = getInt(prefix + "_multiplier")
    val asset = getString(prefix + "_asset")
    return if (asset == null) null else Price(value, multiplier, asset)
}

fun ResultSet.getDateTime(column: String, format: SimpleDateFormat): Date = format.parse(getString(column))