package io.github.iluu.crypto.transactions.domain

enum class TransactionType {
    TRADE,
    DEPOSIT,
    WITHDRAWAL,
    LOST,
    INCOME,
    STOLEN,
    GIFT
}
