package io.github.iluu.crypto.transactions.domain

data class Price(val value: Long, val multiplier: Int, val asset: String = "EUR")