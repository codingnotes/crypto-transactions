package io.github.iluu.crypto.transactions.domain

import java.util.*

data class Transaction(
        val id: Long? = null,
        val tradeTime: Date,
        val importTime: Date? = null,
        val type: TransactionType = TransactionType.TRADE,
        val buyPrice: Price? = null,
        val sellPrice: Price? = null,
        val fee: Price? = null,
        val exchange: String? = null,
        val comment: String? = null,
        val importedFrom: String? = null
)