CREATE TABLE IF NOT EXISTS TRANSACTIONS (
    ID integer primary key asc,
    CREATED_AT datetime not null,
    ADDED_AT datetime not null default current_date,
    TYPE int2 not null,
    BUY_ASSET text(5),
    BUY_VALUE bigint,
    BUY_MULTIPLIER int,
    SELL_ASSET text(5),
    SELL_VALUE bigint,
    SELL_MULTIPLIER int,
    FEE_ASSET text(5),
    FEE_VALUE bigint,
    FEE_MULTIPLIER int,
    EXCHANGE text,
    COMMENT text,
    IMPORTED_FROM text
);